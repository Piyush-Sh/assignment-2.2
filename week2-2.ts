//generic
interface myMap<T>{
    [key :string]:T,
  }
  
  class Fornumber<T> implements myMap<T>{
    [key:string]:T,
  }
  let obj1:myMap<number> = new Fornumber();
  obj1["apple"]=5;
  obj1['bananas']=10;
  console.log(obj1)
  console.log(obj1["apple"]);
  
  let obj2:myMap<string> = new Fornumber();
  obj2["name"]="Max";
  obj2["age"]="27";
  console.log(obj2)

//Decorator
function perDeatials(constructor:Function){
    let lastname="Sharma"
    console.log("last "+lastname);
    console.log(constructor);

}
@perDeatials
class Per{
    name="Piyush"
    constructor(){
        console.log("class "+this.name)
    }
}
const p= new Per();
console.log(p);