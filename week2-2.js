var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var Fornumber = /** @class */ (function () {
    function Fornumber() {
    }
    return Fornumber;
}());
var obj1 = new Fornumber();
obj1["apple"] = 5;
obj1['bananas'] = 10;
console.log(obj1);
console.log(obj1["apple"]);
var obj2 = new Fornumber();
obj2["name"] = "Max";
obj2["age"] = "27";
console.log(obj2);
//Decorator
function perDeatials(constructor) {
    var lastname = "Sharma";
    console.log("last " + lastname);
    console.log(constructor);
}
var Per = /** @class */ (function () {
    function Per() {
        this.name = "Piyush";
        console.log("class " + this.name);
    }
    Per = __decorate([
        perDeatials
    ], Per);
    return Per;
}());
var p = new Per();
console.log(p);
